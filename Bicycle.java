// Alexander Efstathakis 2043441
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String brand, int gears, double topSpeed){
        this.manufacturer = brand;
        this.numberGears = gears;
        this.maxSpeed = topSpeed;
    }
    public String getManufacturer(){
        return manufacturer;
    }

    public int getNumberGears(){
        return numberGears;
    }

    public double getMaxSpeed(){
        return maxSpeed;
    }

    public String toString(){
        return ("Manufacturer: "+this.manufacturer+ ", Number of gears: "+this.numberGears+ ", Max speed: "+this.maxSpeed);
    }
}
